var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var mongoose = require('mongoose');

var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

var app = express();

process.env.NODE_ENV = process.env.WORK_ENV;

app.set('port', process.env.PORT || 4000);
// view engine setup
app.set('views', __dirname + '/app');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');


var config = require('./server/config');
mongoose.connect(config.mongo.uri, function(err){
  if(err)
    console.log("Database Error: "+err);
  else
    console.log("Database Connnected")}
  ,config.mongo.options);


// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'app')));
app.use(passport.initialize());
app.use(passport.session());


app.get('/', function (req, res)
{
  res.render('index.html');
});


var cloudinary = require('cloudinary');
cloudinary.config({
  cloud_name: '',
  api_key: '',
  api_secret: ''
});

//Passport

var User = require('./server/models/user');
passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));
passport.serializeUser(function(user, done) {
  console.log("Serialize User:"+user);
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function (err, user) {
    console.log("Deserialize User:"+user);
    done(err, user);
  });
});
require('./server/event_api')(app);
require('./server/user_api')(app,passport);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'))
});
module.exports = app;
