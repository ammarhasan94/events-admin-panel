/**
 * Created by sohaib on 9/15/2015.
 */
/**
 * Created by sohaib on 2/14/2015.
 */
app.service('dataModels', function () {
  return {
    newEventData : function(){
      return {
        name: '',
        logoURL:'',
        code:'',
        colorTheme:'',
        description:'',
        startDate: new Date(),
        startTime: new Date(),
        endDate: new Date(),
        endTime: new Date()
      }
    }
  };
});
