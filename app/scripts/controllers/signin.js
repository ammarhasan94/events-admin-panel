'use strict';

/**
 * @ngdoc function
 * @name adminPanelApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminPanelApp
 */
app.controller('SignInCtrl', function ($scope, $location)
{
  $scope.screenLoading = true;
  // $scope.user = sharedMethods.getUser();
  if ($scope.user && $scope.user.userType == 'admin')
  {
    $location.path('/admin-dashboard');
  }
  else if($scope.user && $scope.user.userType != 'admin')
  {
    $location.path('/dashboard');
  }
  else
  {
    $scope.screenLoading = false;
  }
  $scope.screenLoading = false;
  $scope.signinerr = false;
  $scope.signindata = {};
  $scope.showspinner = false;


  $scope.signin = function()
  {
    if (!($scope.signindata.email && $scope.signindata.password))
    {
      $scope.signinerr = true;
    }
    else
    {
      $scope.signinerr = false;
      $scope.showspinner = true;
      $location.path('/admin-dashboard');
      /*   $http.post('/api/v2/login', {
       email:       $scope.signindata.email,
       password:    $scope.signindata.password
       })
       .success(function(user){
       $scope.message = 'Logged in successfully';
       $scope.signindata = {};
       $scope.showspinner = false;
       localStorage.setItem("user", JSON.stringify(user));
       if (user.userType=='admin')
       {
       $location.path('/admin-dashboard');
       }
       else if(user.userType=='agent') {
       }
       else
       {
       $scope.message = 'You need admin or agent right to access dashboard';
       }
       })
       .error(function(err){
       $scope.signinerr = true;
       $scope.signindata = {};
       $scope.showspinner = false;
       });*/
    }
  }

});
