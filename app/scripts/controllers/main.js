'use strict';

/**
 * @ngdoc function
 * @name adminPanelApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminPanelApp
 */
app.controller('MainCtrl', function ($scope, dataModels)
  {
    $scope.currentView = {welcome:true};
    $scope.allEvents = [{_id:1, name:'Event 1'},{_id:2, name:'Event 2'},{_id:3, name:'Event 3'},{_id:4, name:'Event 4'}];
    $scope.currentEventId = '';


    $scope.newEventView = function(){
      $scope.currentView = {mainevent:true};
      $scope.eventData = dataModels.newEventData();
    }
    $scope.createEvent = function(){

    }
    $scope.updateEvent = function(){

    }
    $scope.showParticipants = function()
    {

    }

  });
