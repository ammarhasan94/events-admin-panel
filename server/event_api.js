var events = require('./routeHandlers/events');

module.exports = function (app) {
  app.post("/api/events", function (req, res) {
    //if (req.user && req.user.userType == 'admin') {
      events.createEvent(req, res);
    //} else {
    //  res.send("Not authorized for this operation!", 401);
    //}
  });

  app.get("/api/events", function (req, res) {
    events.getAllEvents(req, res);
  });


  app.get("/api/events/:eventId", function (req, res) {
    events.getEventById(req, res);
  });

  app.put("/api/events/:eventId", function (req, res) {
    //if (req.user && req.user.userType == 'admin') {
      events.updateEvent(req, res);
    //} else {
    //  res.send("Not authorized for this operation!", 401);
    //}
  });

  app.delete("/api/events/:eventId", function (req, res) {
    //if (req.user && req.user.userType == 'admin') {
      events.deleteEventById(req, res);
    //} else {
    //  res.send("Not authorized for this operation!", 401);
    //}
  });

}
