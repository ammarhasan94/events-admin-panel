/**
 * Created by sohaib on 1/12/2015.
 */
// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/adminPanel'
  }
};
