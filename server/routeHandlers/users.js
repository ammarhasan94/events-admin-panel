var mongoose = require('mongoose');
var User = require('../models/user');

exports.checkUserNameAvailability = function (req, res) {
  User.findOne({ username:req.params.username }, function (err, user) {
    if (err) return res.send('Bad Request');
    else if (!user) res.send(true);
    else res.send(false)
  });
}
exports.checkEmailAvailability = function (req, res) {
  User.findOne({ username:req.params.email }, function (err, user) {
    if (err) return res.send('Bad Request');
    else if (!user) res.send(true);
    else res.send(false)
  });
}
exports.signUp = function (req, res) {
  User.findOne({ 'email': req.body.email }, function (err, user) {
    // In case of any error, return using the done method
    if (err) {
      console.log('Error in SignUp: ' + err);
      res.send('Error in Signup', 400);
    }
    // already exists
    if (user) {
      console.log('User already exists with email: ' +  req.body.email);
      res.send('User already exists with email: ' +  req.body.email, 400);
    } else {
      // if there is no user with that email
      // create the user
      var newUser = new User();
      // set the user's local credentials
      newUser.username = req.body.username;
      newUser.email = req.body.email;
      newUser.password = req.body.password;
      newUser.firstName = req.body.firstName || '';
      newUser.lastName = req.body.lastName || '';
      newUser.address = req.body.address || '';
      newUser.city =  req.body.city || '';
      newUser.state =  req.body.state || '';
      newUser.country =  req.body.country || '';
      newUser.userType =  req.body.userType || 'agent';
      // save the user
      newUser.save(function (err, user) {
        if (err) {
          console.log('Error in Saving user: ' + err);
          res.send('Error in Saving user', 400);
        }
        console.log('User Registration successful');
        res.send(user, 200);
      });
    }
  });
}
exports.getUserByName = function (req, res) {
  User.findOne({username : req.params.username}, function(err, user) {
    if (err) res.send('User does not exist', 400);
    else
    {
      var obj = updatedUser(req.body);
      delete obj._id;
      User.update({username : req.params.username}, obj, function(err, user)
      {
        if (err)
        {
          res.send('Could not update the document', 400)
        }
        else
        {
          res.send('Updated user successfully', 200);
        }
      });
    }
  });
}
exports.deleteUserByName = function (req, res) {
  User.findOne({username : req.params.username}, function(err, user) {
    if (err) res.send('User does not exist', 400);
    else
    {
      User.remove({ username : req.params.username }, function(err) {
        if (err)
        {
          res.send('Could not delete the user', 400)
        }
        else
        {
          res.send('User deleted successfully', 200);
        }
      });
    }
  });
}

