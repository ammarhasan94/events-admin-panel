var mongoose = require('mongoose');
var Event = require('../models/event');


exports.createEvent = function (req, res) {
  var newEvent = new Event({
    name: req.body.name,
  code: req.body.code,
  logo: req.body.logo,
  decription: req.body.description,
  colorTheme: req.body.colorTheme,
  startDateTime: req.body.startDateTime,
  endDateTime: req.body.endDateTime
  });
  newEvent.save(function (err, event) {
    if (err) {
      console.error(err);
      res.send('Error in Saving event', 400);
    }
    else res.send(event,200);
  });
}
exports.getAllEvents = function (req, res) {
  Event.find({}, function (err, data) {
    if (err)
      res.json(err);
    else
      res.json(data);
  });
}
exports.getEventById = function (req, res) {
  Event.find({_id: req.params.eventId}, function (err, data) {
    if (err)
      res.json(err);
    else
      res.json(data);
  });

}
exports.updateEvent = function (req, res) {
  Event.findOneAndUpdate({_id: req.params.eventId},
    {
      $set: {name: req.body.name},
      $set: {code: req.body.code},
      $set: {logo: req.body.logo},
      $set: {decription: req.body.description},
      $set: {colorTheme: req.body.colorTheme},
      $set: {startDateTime: req.body.startDateTime},
      $set: {endDateTime: req.body.endDateTime}
    },
    function (err, data) {
      if (err) {
        console.error(err);
        return res.status(500);
      }
      else {
        res.status(200);
        res.json({success: true});
      }
    });
}
exports.deleteEventById = function (req, res) {
  Event.findOneAndRemove({_id: req.body.id},function(err){
    if(err){
      console.error(err);
      res.status(500);
      return res.json({success:false});
    }
    else{
      return res.json({success:true});
    }
  });
}

