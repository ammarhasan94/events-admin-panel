//var users = require('./routeHandlers/users');
var mongoose = require('mongoose');
var User = require('./models/user');
var users= require('./routeHandlers/users');

module.exports = function(app, passport)
{
  app.get('/api/user/checkUserNameAvailability/:username', function(req,res){
   users.checkUserNameAvailability(req,res);
  });

  app.get('/api/user/checkEmailAvailability/:email', function(req,res){
    users.checkEmailAvailability(req,res);
  });

  /* Handle Login */
  app.post('/api/login', passport.authenticate('local'), function(req,res)
  {
    console.log(req.user);
    res.send(req.user);
  });

  app.post('/api/signup', function(req, res)
  {
    if (req.user && req.user.userType == 'admin')
    {
      users.signUp(req,res)
    }
    else
    {
      res.send('Unauthorized', 400);
    }

  });

  app.put('/user/:username', function(req, res)
  {
    if (req.params.username == req.user.username || req.user.userType == 'admin')
    {
      users.getUserByName(req,res);
    }
    else
    {
      res.send('Unauthorized Request', 400)
    }
  });
  /* Delete User */
  app.delete('/user/:username', function(req, res)
  {
    if (req.params.username == req.user.username || req.user.userType == 'admin')
    {
      users.deleteUserByName(req,res);
    }
    else
    {
      res.send('Unauthorized Request', 400)
    }
  });


}
