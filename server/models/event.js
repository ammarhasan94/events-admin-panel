/**
 * Created by sohaib on 1/12/2015.
 */
var mongoose = require('mongoose');

module.exports = mongoose.model('Event', {
  name: {type: String, allowNull: false},
  code: {type: String, allowNull: false},
  logo: {type: String, default:''},
  description: {type: String, default:''},
  colorTheme:{type: String, default:'blue'},
  startDateTime:{ type: Date, default: Date.now },
  endDateTime:{ type: Date, default: Date.now }
  //creator:{type: mongoose.Schema.Types.ObjectId, ref: 'User'}
  //participants: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
});
